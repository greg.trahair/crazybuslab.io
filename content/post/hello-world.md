+++
date = "2017-03-11T23:08:48+01:00"
title = "hello world"
tags = ['python', 'development']
+++

Hello World!

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Is it a greeting or a call for help?"""


def hello_world():
    """Returns a greeting.

    >>> hello_world()
    'Hello, world!'

    """
    return 'Hello, world!'


if __name__ == '__main__':
    import doctest
    doctest.testmod()

    print(hello_world())
```
